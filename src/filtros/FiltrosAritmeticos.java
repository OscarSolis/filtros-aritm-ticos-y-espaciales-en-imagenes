/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filtros;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author korn-28
 */
public class FiltrosAritmeticos extends javax.swing.JFrame {

    //Variables que contendran las images a usar
    private BufferedImage imgA;
    private BufferedImage imgB;
    private int bandera = 0;

    public FiltrosAritmeticos() {
        initComponents();
        opciones.setEnabled(false);
    }

    /**
     * Metodo que rellena tres matrices con cada uno de los colores rgb de la imagen respectivamente
     * @param red matriz para colores rojos
     * @param green matriz para colores verdes
     * @param blue matriz para colores azules
     * @param imagen imagen de la cual se sacaran los colores para rellenar las anteriores matrices
     */
    private void convertirImagenMatriz(int red[][], int green[][], int blue[][], BufferedImage imagen) {
        for (int i = 0; i < imagen.getWidth(); i++) {
            for (int j = 0; j < imagen.getHeight(); j++) {
                Color colorAux = new Color(imagen.getRGB(i, j));
                red[i][j] = colorAux.getRed();
                green[i][j] = colorAux.getGreen();
                blue[i][j] = colorAux.getBlue();
            }
        }
    }
    /**
     * Metodo que convierte 3 matrices a una imagen
     * @param cred matriz de colores rojos
     * @param cgreen matriz de colores verdes
     * @param cblue matriz de colores azules
     * @return imagen nueva
     */
    private BufferedImage convertirMatricesImagen(int cred[][],int cgreen[][],int cblue[][]){
        BufferedImage c = new BufferedImage(cred.length, cred[0].length, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < c.getWidth(); i++) {
            for (int j = 0; j < c.getHeight(); j++) {
                int color24bits = (cred[i][j] << 16) | (cgreen[i][j] << 8) | cblue[i][j];
                c.setRGB(i, j, color24bits);
            }
        }
        return c;
    }
    /**
     * Metodo que realiza la suma de dos imagenes.
     * @param imagen a sumar con imagen2
     * @param imagen2 a sumar con imagen
     * @return 
     */
    private BufferedImage suma(BufferedImage imagen, BufferedImage imagen2) {
        //matrices para la imagen 1
        int Ired[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Iblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Igreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        //matrices para la imagen 2
        int Ired2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        int Iblue2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        int Igreen2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        //matrices que contendran los valores de la nueva imagen
        int cred[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int cgreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int cblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        //rellenado de las 6 matrices
        convertirImagenMatriz(Ired, Igreen, Iblue, imagen);
        convertirImagenMatriz(Ired2, Igreen2, Iblue2, imagen2);
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                cred[i][j] = Ired[i][j] + Ired2[i][j];

                cgreen[i][j] = Igreen[i][j] + Igreen2[i][j];

                cblue[i][j] = Iblue[i][j] + Iblue2[i][j];
            }
        }
        return convertirMatricesImagen(cred, cgreen, cblue);
    }
    /**
     * Metodo que realiza la resta de dos imagenes.
     * @param imagen a restar con imagen2
     * @param imagen2 a restar con imagen
     * @return 
     */
    private BufferedImage resta(BufferedImage imagen, BufferedImage imagen2) {
        //matrices para la imagen 1
        int Ired[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Iblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Igreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        //matrices para la imagen 2
        int Ired2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        int Iblue2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        int Igreen2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        //matrices que contendran los valores de la nueva imagen
        int cred[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int cgreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int cblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        //rellenado de las 6 matrices
        convertirImagenMatriz(Ired, Igreen, Iblue, imagen);
        convertirImagenMatriz(Ired2, Igreen2, Iblue2, imagen2);
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                cred[i][j] = Ired[i][j] - Ired2[i][j];
                cgreen[i][j] = Igreen[i][j] - Igreen2[i][j];
                cblue[i][j] = Iblue[i][j] - Iblue2[i][j];
            }
        }
        return convertirMatricesImagen(cred, cgreen, cblue);
    }
    /**
     * Metodo que realiza la resta de dos imagenes.
     * @param imagen a restar con imagen2
     * @param imagen2 a restar con imagen
     * @return 
     */
    private BufferedImage multiplicacion(BufferedImage imagen, BufferedImage imagen2) {
        //matrices para la imagen 1
        int Ired[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Iblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Igreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        //matrices para la imagen 2
        int Ired2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        int Iblue2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        int Igreen2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        //matrices que contendran los valores de la nueva imagen
        int cred[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int cgreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int cblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        //rellenado de las 6 matrices
        convertirImagenMatriz(Ired, Igreen, Iblue, imagen);
        convertirImagenMatriz(Ired2, Igreen2, Iblue2, imagen2);
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                cred[i][j] = Ired[i][j] * Ired2[i][j];
                cgreen[i][j] = Igreen[i][j] * Igreen2[i][j];
                cblue[i][j] = Iblue[i][j] * Iblue2[i][j];
            }
        }
        return convertirMatricesImagen(cred, cgreen, cblue);
    }
    /**
     * Metodo que realiza la dividir de dos imagenes.
     * @param imagen a dividir con imagen2
     * @param imagen2 a dividir con imagen
     * @return 
     */
    private BufferedImage division(BufferedImage imagen, BufferedImage imagen2) {
        //matrices para la imagen 1
        int Ired[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Iblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Igreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        //matrices para la imagen 2
        int Ired2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        int Iblue2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        int Igreen2[][] = new int[imagen2.getWidth()][imagen2.getHeight()];
        //matrices que contendran los valores de la nueva imagen
        int cred[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int cgreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int cblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        //rellenado de las 6 matrices
        convertirImagenMatriz(Ired, Igreen, Iblue, imagen);
        convertirImagenMatriz(Ired2, Igreen2, Iblue2, imagen2);
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                //si el dividendo es 0 se realiza la divicion por un escalar
                if (Ired2[i][j] != 0) {
                    cred[i][j] = Ired[i][j] / Ired2[i][j];
                } else {
                    cred[i][j] = Ired[i][j] / 2;
                }
                if (Igreen2[i][j] != 0) {
                    cgreen[i][j] = Igreen[i][j] / Igreen2[i][j];
                } else {
                    cgreen[i][j] = Igreen[i][j] / 2;
                }
                if (Iblue2[i][j] != 0) {
                    cblue[i][j] = Iblue[i][j] / Iblue2[i][j];
                } else {
                    cblue[i][j] = Iblue[i][j] / 2;
                }
            }
        }
        return convertirMatricesImagen(cred, cgreen, cblue);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        imagenA = new javax.swing.JLabel();
        imagenB = new javax.swing.JLabel();
        imagenC = new javax.swing.JLabel();
        opciones = new javax.swing.JComboBox();
        cargarImagenA = new javax.swing.JButton();
        cargarImagenB = new javax.swing.JButton();
        salir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 300));

        imagenA.setText("     POR FAVOR ELIJA DOS IMAGENES CON EL MISMO TAMAÑO");
        imagenA.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        imagenB.setText("     POR FAVOR ELIJA DOS IMAGENES CON EL MISMO TAMAÑO");
        imagenB.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        imagenC.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        opciones.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione una opcion", "Suma", "Resta", "Multiplicacion", "Divicion" }));
        opciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionesActionPerformed(evt);
            }
        });

        cargarImagenA.setText("Cargar imagen A");
        cargarImagenA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargarImagenAActionPerformed(evt);
            }
        });

        cargarImagenB.setText("Cargar imagen B");
        cargarImagenB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargarImagenBActionPerformed(evt);
            }
        });

        salir.setText("Salir");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(imagenA, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(imagenB, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(cargarImagenA)
                        .addGap(303, 303, 303)
                        .addComponent(cargarImagenB)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(opciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(70, 70, 70)
                        .addComponent(salir))
                    .addComponent(imagenC, javax.swing.GroupLayout.PREFERRED_SIZE, 409, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(29, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(imagenC, javax.swing.GroupLayout.DEFAULT_SIZE, 429, Short.MAX_VALUE)
                    .addComponent(imagenA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(imagenB, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cargarImagenA, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cargarImagenB, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(opciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salir))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1330, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * Metodo que carga una imagen A
     * @param evt 
     */
    private void cargarImagenBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cargarImagenBActionPerformed
        File fichero;
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG", "jpg");
        fileChooser.setFileFilter(filtroImagen);
        int r = fileChooser.showOpenDialog(this);
        if (r == JFileChooser.APPROVE_OPTION) {
            fichero = fileChooser.getSelectedFile();
            try {
                ImageIcon icon = new ImageIcon(fichero.toString());
                Icon icono = new ImageIcon(icon.getImage().getScaledInstance(imagenB.getWidth(), imagenB.getHeight(), Image.SCALE_DEFAULT));
                imagenB.setIcon(icono);
                imgB = ImageIO.read(fichero);
                bandera++;
                if (bandera > 1) {
                    opciones.setEnabled(true);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error abriendo la imagen " + ex);
            }
        }
    }//GEN-LAST:event_cargarImagenBActionPerformed
    /**
     * Metodo que carga una imagen B
     * @param evt 
     */
    private void cargarImagenAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cargarImagenAActionPerformed
        File fichero;
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG", "jpg");
        fileChooser.setFileFilter(filtroImagen);
        int r = fileChooser.showOpenDialog(this);
        if (r == JFileChooser.APPROVE_OPTION) {
            fichero = fileChooser.getSelectedFile();
            try {
                ImageIcon icon = new ImageIcon(fichero.toString());
                Icon icono = new ImageIcon(icon.getImage().getScaledInstance(imagenA.getWidth(), imagenB.getHeight(), Image.SCALE_DEFAULT));
                imagenA.setIcon(icono);
                imgA = ImageIO.read(fichero);
                bandera++;
                if (bandera > 1) {
                    opciones.setEnabled(true);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error abriendo la imagen " + ex);
            }
        }
    }//GEN-LAST:event_cargarImagenAActionPerformed
    /**
     * Obcion por filtro a seleccionar
     * @param evt listener del jcombobox
     */
    private void opcionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionesActionPerformed
        int opcion = opciones.getSelectedIndex();
        BufferedImage c;
        switch (opcion) {
            case 0:
                imagenC.setIcon(null);
                imagenC.setText("                   seleccione un filtro");
                break;
            case 1:
                c = suma(imgA, imgB);
                imagenC.setIcon(new ImageIcon(c.getScaledInstance(
                        imagenC.getWidth(), imagenC.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 2:
                c = resta(imgA, imgB);
                imagenC.setIcon(new ImageIcon(c.getScaledInstance(
                        imagenC.getWidth(), imagenC.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 3:
                c = multiplicacion(imgA, imgB);
                imagenC.setIcon(new ImageIcon(c.getScaledInstance(
                        imagenC.getWidth(), imagenC.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 4:
                c = division(imgA, imgB);
                imagenC.setIcon(new ImageIcon(c.getScaledInstance(
                        imagenC.getWidth(), imagenC.getHeight(), Image.SCALE_DEFAULT)));
                break;
            default: JOptionPane.showMessageDialog(null, "Opcion invalida");
                
        }
    }//GEN-LAST:event_opcionesActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        this.setVisible(false);
        Principal p = new Principal();
        p.setVisible(true);
    }//GEN-LAST:event_salirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FiltrosAritmeticos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FiltrosAritmeticos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FiltrosAritmeticos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FiltrosAritmeticos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FiltrosAritmeticos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cargarImagenA;
    private javax.swing.JButton cargarImagenB;
    private javax.swing.JLabel imagenA;
    private javax.swing.JLabel imagenB;
    private javax.swing.JLabel imagenC;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JComboBox opciones;
    private javax.swing.JButton salir;
    // End of variables declaration//GEN-END:variables
}
