/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filtros;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


/**
 *
 * @author korn-28
 */
public class Filtros extends javax.swing.JFrame {

    //Variable que contendra la imagen a procesar
    
    private BufferedImage imagenOriginal;

    /**
     * Creates new form Filtros
     */
    public Filtros() {
        initComponents();
        opciones.setEnabled(false);

    }

    /**
     * Metodo que convierte una BufferedImage a una matriz de datos enteros
     *
     * @param imagen a convertir en matriz
     * @return matriz con datos enteros de la imagen
     */
    private int[][] convertirImagenMatriz(BufferedImage imagen) {
        //creamos una matriz la cual almacenara los valores de la imagen
        int In[][] = new int[imagen.getWidth()][imagen.getHeight()];
        for (int i = 0; i < imagen.getWidth(); i++) {
            for (int j = 0; j < imagen.getHeight(); j++) {
                //extraemos los colores RGB de la imagen en cada pixel
                Color colorAux = new Color(imagen.getRGB(i, j));
                int color = (int) ((colorAux.getRed() + colorAux.getGreen() + colorAux.getBlue()) / 3);
                //agregamos el valor del pixel a la matriz
                In[i][j] = color;
            }
        }
        return In;
    }

    /**
     * Metodo que convierte una matriz a un BufferedImage imagen
     *
     * @param In matriz a convertir en imagen
     * @return nueva imagen
     */
    private BufferedImage convertirMatrizImagen(int In[][]) {
        BufferedImage bufferImage2 = new BufferedImage(In.length, In[0].length, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < imagenOriginal.getWidth(); i++) {
            for (int j = 0; j < imagenOriginal.getHeight(); j++) {
                int colorSRGB = (In[i][j] << 16) | (In[i][j] << 8) | In[i][j];
                bufferImage2.setRGB(i, j, colorSRGB);
            }
        }
        return bufferImage2;
    }

    /**
     * Metodo que asigna llena de valores a cada matriz del modelo RGB
     *
     * @param Ired matriz a rellenar con valores Rojos
     * @param Igreen matriz a rellenar con valores verdes
     * @param Iblue matriz a rellenar con valores azules
     * @param imagen de donde se sacaran los valores para rellenar las matrices
     * Ired, Iblue, Igreen
     */
    private void matrices(int Ired[][], int Igreen[][], int Iblue[][], BufferedImage imagen) {
        for (int i = 0; i < imagen.getWidth(); i++) {
            for (int j = 0; j < imagen.getHeight(); j++) {
                Color colorAux = new Color(imagen.getRGB(i, j));
                Ired[i][j] = colorAux.getRed();
                Iblue[i][j] = colorAux.getBlue();
                Igreen[i][j] = colorAux.getGreen();
            }
        }
    }

    /**
     * Metodo que convierte 3 matrices a una imagen
     *
     * @param Ired matriz de colores rojos
     * @param Igreen matriz de colores verdes
     * @param Iblue matriz de colores azules
     * @return
     */
    private BufferedImage matrizImagen(int Ired[][], int Igreen[][], int Iblue[][]) {
        BufferedImage bufferImage2 = new BufferedImage(Ired.length, Ired[0].length, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < imagenOriginal.getWidth(); i++) {
            for (int j = 0; j < imagenOriginal.getHeight(); j++) {
                int color24bits = (Ired[i][j] << 16) | (Igreen[i][j] << 8) | Iblue[i][j];
                bufferImage2.setRGB(i, j, color24bits);
            }
        }
        return bufferImage2;
    }

    /**
     * Metodo que saca el filtroMedia de una imagen
     *
     * @param imagen a aplicar el filtro el filtro
     * @return la nueva imagen
     */
    private BufferedImage filtroMedia(BufferedImage imagen) {
        //declaracion de 3 matrices una para cada color RGBs
        int Ired[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Iblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Igreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        matrices(Ired, Igreen, Iblue, imagen);
        int mediaRojo, mediaVerde, mediaAzul;
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                mediaRojo = 0;
                mediaVerde = 0;
                mediaAzul = 0;
                //for's que sumaran los vecinos 8 del pixel (x,y)
                for (int x = i - 1; x <= i + 1; x++) {
                    for (int y = j - 1; y <= j + 1; y++) {
                        mediaRojo += Ired[x][y];
                        mediaVerde += Igreen[x][y];
                        mediaAzul += Iblue[x][y];
                    }
                }
                Ired[i][j] = (mediaRojo / 9);
                Igreen[i][j] = (mediaVerde / 9);
                Iblue[i][j] = (mediaAzul / 9);
            }
        }
        return matrizImagen(Ired, Igreen, Iblue);
    }

    /**
     * Metodo que realiza el filtro de la mediana a una imagen
     *
     * @param imagen imagen aplicar el filtro
     * @return imagen procesada
     */
    private BufferedImage filtroMediana(BufferedImage imagen) {
        int Ired[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Iblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Igreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int vecRed[] = new int[9], vecGreen[] = new int[9], vecBlue[] = new int[9];
        matrices(Ired, Igreen, Iblue, imagen);
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                int cont = 0;
                for (int x = i - 1; x <= i + 1; x++) {
                    for (int y = j - 1; y <= j + 1; y++) {
                        //guardar los valores en cada vector con respecto a su color
                        vecRed[cont] = Ired[x][y];
                        vecGreen[cont] = Igreen[x][y];
                        vecBlue[cont] = Iblue[x][y];
                        cont++;
                    }
                }
                //ordenamiento de los vectores
                shellSort(vecRed);
                shellSort(vecGreen);
                shellSort(vecBlue);
                //asignacion de los valores centrales del vector ordenado, en cada matriz con respeto a su color
                Ired[i][j] = vecRed[4];
                Igreen[i][j] = vecGreen[4];
                Iblue[i][j] = vecBlue[4];
            }
        }
        return matrizImagen(Ired, Igreen, Iblue);
    }

    /**
     * Metodo que realiza la deteccion de bordes con el filtro prewitt
     *
     * @param imagen a procesar
     * @return una matriz con los valores de la nueva imagen
     */
    public int[][] filtroPrewitt(BufferedImage imagen) {
        int[][] imagenNueva = new int[imagen.getWidth()][imagen.getHeight()];
        int[][] imagenOriginal = convertirImagenMatriz(imagen);
        int pixel_x, pixel_y, cont, cont2;
        //mascaras de convolucion de prewitt
        int[][] prewitt_x = {{1, 0, -1},
                             {1, 0, -1},
                             {1, 0, -1}};
        int[][] prewitt_y = {{-1,-1,-1},
                              {0, 0, 0},
                              {1, 1, 1}};
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                cont = 0;
                pixel_x = 0;
                pixel_y = 0;
                for (int x = i - 1; x <= i + 1; x++) {
                    cont2 = 0;
                    for (int y = j - 1; y <= j + 1; y++) {
                        pixel_x += (prewitt_x[cont][cont2] * imagenOriginal[x][y]);
                        pixel_y += (prewitt_y[cont][cont2] * imagenOriginal[x][y]);
                        cont2++;
                    }
                    cont++;
                }
                //se saca la raiz cuadrada de la suma de pixel_x al cuadrado + pixel_y al cuadrado
                int val = (int) Math.sqrt((pixel_x * pixel_x) + (pixel_y * pixel_y));
                //si el valor es menor a 0 se cambia dicho valor por 0
                if (val < 0) {
                    val = 0;
                }
                //si el valor es mayor a 255 se cambia el valor por 255
                if (val > 255) {
                    val = 255;
                }
                imagenNueva[i][j] = val;
            }
        }
        return imagenNueva;
    }

    /**
     * Metodo que realiza la deteccion de bordes con el filtro prewitt
     * @param imagen a procesar
     * @return una matriz con los valores de la nueva imagen
     */
    public int[][] filtroSobel(BufferedImage imagen) {
        int[][] imagenNueva = new int[imagen.getWidth()][imagen.getHeight()];
        int[][] imagenOriginal = convertirImagenMatriz(imagen);
        int pixel_x, pixel_y, cont, cont2;
        //mascaras de convulucion del filtro sobel
        int sobel_x[][] = {{-1, 0, 1},
                           {-2, 0, 2},
                           {-1, 0, 1}};
        int sobel_y[][] = {{-1, -2, -1},
        {0, 0, 0},
        {1, 2, 1}};
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                cont = 0;
                pixel_x = 0;
                pixel_y = 0;
                for (int x = i - 1; x <= i + 1; x++) {
                    cont2 = 0;
                    for (int y = j - 1; y <= j + 1; y++) {
                        pixel_x += (sobel_x[cont][cont2] * imagenOriginal[x][y]);
                        pixel_y += (sobel_y[cont][cont2] * imagenOriginal[x][y]);
                        cont2++;
                    }
                    cont++;
                }
                //se saca la raiz cuadrada de la suma de pixel_x al cuadrado + pixel_y al cuadrado
                int val = (int) Math.sqrt((pixel_x * pixel_x) + (pixel_y * pixel_y));
                //si el valor es menor a 0 se cambia dicho valor por 0
                if (val < 0) {
                    val = 0;
                }
                //si el valor es mayor a 255 se cambia el valor por 255
                if (val > 255) {
                    val = 255;
                }
                imagenNueva[i][j] = val;
            }
        }
        return imagenNueva;
    }

    /**
     * Metodo que realiza la deteccion de bordes con el filtro roberts
     * @param imagen a procesar
     * @return una matriz con los valores de la nueva imagen
     */
    public int[][] filtroRoberts(BufferedImage imagen) {
        int[][] imagenNueva = new int[imagen.getWidth()][imagen.getHeight()];
        int[][] imagenOriginal = convertirImagenMatriz(imagen);
        int pixel_x, pixel_y;
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                pixel_x = imagenOriginal[i][j] - imagenOriginal[i - 1][j - 1];
                pixel_y = imagenOriginal[i][j - 1] - imagenOriginal[i - 1][j];
                //se saca la raiz cuadrada de la suma de pixel_x al cuadrado + pixel_y al cuadrado
                int val = (int) Math.sqrt((pixel_x * pixel_x) + (pixel_y * pixel_y));
                //si el valor es menor a 0 se cambia dicho valor por 0
                if (val < 0) {
                    val = 0;
                }
                //si el valor es mayor a 255 se cambia el valor por 255
                if (val > 255) {
                    val = 255;
                }
                imagenNueva[i][j] = val;
            }
        }
        return imagenNueva;
    }

    /**
     * Metodo que calcula el gradiente de una imagen
     * @param imagen a calcular el gradiente
     * @return matriz con los nuevos valores
     */
    private int[][] gradiente(BufferedImage imagen) {
        int In[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int I[][] = convertirImagenMatriz(imagen);
        int Gx, Gy;
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = i; j < imagen.getHeight() - 1; j++) {
                //suma los pixeles vecinos para Gx y GY
                Gx = (I[i + 1][j + 1] + (2 * I[i][j + 1]) + I[i - 1][j + 1]) - (I[i + 1][j - 1] + (2 * I[i][j - 1]) + I[i - 1][j - 1]);
                Gy = (I[i + 1][j - 1] + (2 * I[i + 1][j]) + I[i + 1][j + 1]) - (I[i - 1][j - 1] + (2 * I[i - 1][j]) + I[i - 1][j + 1]);
                int G = Math.abs(Gx + Gy);
                
                if( G > 255){
                    In[i][j] = 255;
                }else if(G <  0){
                    In[i][j] = 0;
                }else{
                    In[i][j] = G;
                }
            }
        }
        return In;
    }

    /**
     *
     * @param imagen a calcular su inversa
     * @return la nueva imagen con los colores invertidos
     */
    public BufferedImage filtroInvertir(BufferedImage imagen) {
        BufferedImage nuevaImagen = new BufferedImage(imagen.getWidth(), imagen.getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < imagen.getWidth(); i++) {
            for (int j = 0; j < imagen.getHeight(); j++) {
                //obtenemos los colores en el pixel (x,y)
                Color color = new Color(imagen.getRGB(i, j));
                int r = color.getRed();
                int g = color.getGreen();
                int b = color.getBlue();
                //restamos 255 a cada color para sacar su negativo
                nuevaImagen.setRGB(i, j, new Color(255 - r, 255 - g, 255 - b).getRGB());
            }
        }
        return nuevaImagen;
    }
    /**
     * 
     * @param imagen imagen aplicar el ruido
     * @return imagen nueva con ruido aplicado
     */
    private BufferedImage ruido(BufferedImage imagen) {
        //declaracion de 3 matrices una para cada color RGBs
        int Ired[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Iblue[][] = new int[imagen.getWidth()][imagen.getHeight()];
        int Igreen[][] = new int[imagen.getWidth()][imagen.getHeight()];
        matrices(Ired, Igreen, Iblue, imagen);
        for (int i = 1; i < imagen.getWidth() - 1; i++) {
            for (int j = 1; j < imagen.getHeight() - 1; j++) {
                //multiplicar el valor de la imagen en cada color por un escalar
                //en este caso 2.
                Ired[i][j] = Ired[i][j] * 2;
                Igreen[i][j] = Igreen[i][j] * 2;
                Iblue[i][j] = Iblue[i][j] * 2;
            }
        }
        return matrizImagen(Ired, Igreen, Iblue);
    }

    /**
     * Metoto de ordenamiento shellsort
     * @param a que es el vector a ordenar
     */
    public void shellSort(int a[]) {
        for (int gap = a.length / 2; gap > 0; gap = gap == 2 ? 1 : (int) (gap / 2.2)) {
            for (int i = gap; i < a.length; i++) {
                int tmp = a[i];
                int j;
                for (j = i; j >= gap && tmp < a[j - gap]; j -= gap) {
                    a[j] = a[j - gap];
                }
                a[j] = tmp;
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jlabelImagen = new javax.swing.JLabel();
        jlabelNewImagen = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        opciones = new javax.swing.JComboBox();
        Salir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jlabelImagen.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jlabelNewImagen.setText("                                  Seleccione una imagen");
        jlabelNewImagen.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jlabelImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 497, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabelNewImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jlabelNewImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jlabelImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jButton1.setText("Cargar Imagen");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        opciones.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione un filtro", "Filtro media", "Filtro Mediana", "Sobel", "Prewitt", "Roberts", "Negativo", "Gradiente", "Ruido aleatorio" }));
        opciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionesActionPerformed(evt);
            }
        });

        Salir.setText("Salir");
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(opciones, 0, 177, Short.MAX_VALUE)
                    .addComponent(Salir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(opciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Salir)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * Metodo que carga una imagen
     * @param evt listener
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //fichero que traera la imagen en la ruta especificada por el JFileChooser
        File fichero;
        JFileChooser fileChooser = new JFileChooser();
        //abrimos el JFileChooser para buscar la imagen a procesar
        int r = fileChooser.showOpenDialog(this);
        if (r == JFileChooser.APPROVE_OPTION) {
            //obtenemos la imagen seleccionada
            fichero = fileChooser.getSelectedFile();
            try {
                //mostramos la imagen en el JLabel
                ImageIcon icon = new ImageIcon(fichero.toString());
                Icon icono = new ImageIcon(icon.getImage().getScaledInstance(
                        jlabelImagen.getWidth(), jlabelImagen.getHeight(), Image.SCALE_DEFAULT));
                jlabelImagen.setIcon(icono);
                //guardamos la imagen en un BufferedImage para poder manipularla
                imagenOriginal = ImageIO.read(fichero);
                opciones.setEnabled(true);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error abriendo la imagen " + ex);
            }
        }

    }//GEN-LAST:event_jButton1ActionPerformed
    /**
     * Metodo que cargara las opciones con respecto a cada filtro
     * @param evt 
     */
    private void opcionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionesActionPerformed

        int opcion = opciones.getSelectedIndex();
        int In[][];
        BufferedImage bufferedImage2;
        switch (opcion) {
            case 0:
                jlabelNewImagen.setIcon(null);
                jlabelNewImagen.setText("                   Seleccione un filtro");
                break;
            case 1:
                bufferedImage2 = filtroMedia(imagenOriginal);
                jlabelNewImagen.setIcon(new ImageIcon(bufferedImage2.getScaledInstance(
                        jlabelNewImagen.getWidth(), jlabelNewImagen.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 2:
                bufferedImage2 = filtroMediana(imagenOriginal);
                jlabelNewImagen.setIcon(new ImageIcon(bufferedImage2.getScaledInstance(
                        jlabelNewImagen.getWidth(), jlabelNewImagen.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 3:
                In = filtroSobel(imagenOriginal);
                bufferedImage2 = convertirMatrizImagen(In);
                jlabelNewImagen.setIcon(new ImageIcon(bufferedImage2.getScaledInstance(
                        jlabelNewImagen.getWidth(), jlabelNewImagen.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 4:
                In = filtroPrewitt(imagenOriginal);
                bufferedImage2 = convertirMatrizImagen(In);
                jlabelNewImagen.setIcon(new ImageIcon(bufferedImage2.getScaledInstance(
                        jlabelNewImagen.getWidth(), jlabelNewImagen.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 5:
                In = filtroRoberts(imagenOriginal);
                bufferedImage2 = convertirMatrizImagen(In);
                jlabelNewImagen.setIcon(new ImageIcon(bufferedImage2.getScaledInstance(
                        jlabelNewImagen.getWidth(), jlabelNewImagen.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 6:
                bufferedImage2 = filtroInvertir(imagenOriginal);
                jlabelNewImagen.setIcon(new ImageIcon(bufferedImage2.getScaledInstance(
                        jlabelNewImagen.getWidth(), jlabelNewImagen.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 7:
                In = gradiente(imagenOriginal);
                bufferedImage2 = convertirMatrizImagen(In);
                jlabelNewImagen.setIcon(new ImageIcon(bufferedImage2.getScaledInstance(
                        jlabelNewImagen.getWidth(), jlabelNewImagen.getHeight(), Image.SCALE_DEFAULT)));
                break;
            case 8:
                bufferedImage2 = ruido(imagenOriginal);
                jlabelNewImagen.setIcon(new ImageIcon(bufferedImage2.getScaledInstance(
                        jlabelNewImagen.getWidth(), jlabelNewImagen.getHeight(), Image.SCALE_DEFAULT)));
                break;
        }
    }//GEN-LAST:event_opcionesActionPerformed

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
        this.setVisible(false);
        Principal p = new Principal();
        p.setVisible(true);
    }//GEN-LAST:event_SalirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Filtros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Filtros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Filtros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Filtros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Filtros().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Salir;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel jlabelImagen;
    private javax.swing.JLabel jlabelNewImagen;
    private javax.swing.JComboBox opciones;
    // End of variables declaration//GEN-END:variables
}
